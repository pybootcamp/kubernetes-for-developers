resource "google_compute_network" "private" {
  name                    = "private-${var.environment}"
  auto_create_subnetworks = false
}

# subnetwork for the Kubernetes cluster
resource "google_compute_subnetwork" "k8s-subnetwork" {
  name                     = "k8s-subnetwork"
  ip_cidr_range            = var.k8s_ip_cidr_range
  region                   = var.region
  network                  = google_compute_network.private.self_link
  private_ip_google_access = true

}

resource "google_compute_global_address" "k8s-loadbalancer-ip" {
  name = "k8s-loadbalancer-${var.environment}"
}

output "ip-address" {
  value = google_compute_global_address.k8s-loadbalancer-ip.address
}
