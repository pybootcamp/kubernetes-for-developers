resource "kubernetes_ingress" "k8s-ingress" {
  metadata {
    name = google_container_cluster.primary.name
    annotations = {
      "kubernetes.io/ingress.global-static-ip-name" = google_compute_global_address.k8s-loadbalancer-ip.name
      "kubernetes.io/ingress.allow-http"            = true
    }
  }

  spec {

    rule {
      http {
        path {
          backend {
            service_name = "basil-web"
            service_port = 8080
          }

          path = "/*"
        }

      }
    }

  }
}