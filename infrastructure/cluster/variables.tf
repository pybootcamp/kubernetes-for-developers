# ---------------------------------------------------------------------------------------------------------------------
# PROJECT VARIABLES
# ---------------------------------------------------------------------------------------------------------------------

variable "project" {
  description = "GCP project ID"
  type        = string
}

variable "region" {
  description = "GCP region"
  type        = string
}

variable "zone" {
  description = "GCP zone"
  type        = string
}

variable "environment" {
  description = "Name of the environment"
  type        = string
}

# ---------------------------------------------------------------------------------------------------------------------
# CLUSTER VARIABLES
# ---------------------------------------------------------------------------------------------------------------------

variable "cluster_name" {
  default = "k8s-cluster"
  type        = string
}

variable "initial_node_count" {
  default = 1
  type        = number
}

variable "node_machine_type" {
  default = "n1-standard-1"
  type        = string
}

variable "node_disk_size" {
  default = 20
  type        = number

}

variable "autoscale_min" {
  default = 1
  type        = number
}

variable "autoscale_max" {
  default = 1
  type        = number
}

# ---------------------------------------------------------------------------------------------------------------------
# NETWORK VARIABLES
# ---------------------------------------------------------------------------------------------------------------------

variable "k8s_ip_cidr_range" {
  default = "10.128.0.0/20"
  type        = string
}
