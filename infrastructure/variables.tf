# ---------------------------------------------------------------------------------------------------------------------
# PROJECT VARIABLES
# ---------------------------------------------------------------------------------------------------------------------

variable "project" {
  description = "GCP project ID"
  type        = string
}

variable "region" {
  description = "GCP region"
  type        = string
}

variable "zone" {
  description = "GCP zone"
  type        = string
}

variable "environment" {
  description = "Name of the environment"
  type        = string
}
