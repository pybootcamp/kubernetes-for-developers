# Basil

--

### Basil

`Basil` is a python application that:

- appends events to a Redis list (through api and a job)
- returns events inside a Redis list

--

Files and folders inside `basil` repository:

```bash
Dockerfile
docker-compose.yaml
infrastructure
poetry.lock
pyproject.toml
src
tests
```

--

To start `basil` on your local machine:

```bash
docker-compose up -d
```

--

you can also run `basil` outside a container (you need to activate your venv):

```
docker-compose up -d redis
uvicorn src.basil.main:app --reload --port 8080
```

--

And create a new event inside:

```bash
 python -m src.basil.job
```

Check:

- http://localhost:8080/events
- http://localhost:8080/docs

--

### Dockerfile

The `Dockerfile` specifies the way in which we build docker images.

Our `Dockerfile` uses multi stage builds and we end up with a relatively tiny image ~353MB.

--

### docker-compose.yaml

`docker-compose.yaml` is used by `docker compose`, it's a way to orchestrate containers on your local machine (and also in production).

2 services are defined there, `basil` and `redis`.

--

The most exotic thing inside the `docker-compose.yaml` is probably

```bash
pybootcamp/basil:${TAG}
```

we can specify that `TAG` using env variables

```
env TAG=0.1 docker-compose up -d
```

--

### pyproject.toml & poetry.lock

`pyproject.toml` comes from [PEP 518](https://www.python.org/dev/peps/pep-0518/) as a way to `speficy minimum build system requirements` for your Python project.

It's also the file used by `Poetry` to specifiy your dependencies.

`poetry.lock` is where those dependencies are pinned, think about it like `package-lock.json`.

--

### src & tests folder

The folder for our application and tests.

--

# Q&A Session
