# Terraform

--

> Terraform is an open-source **infrastructure as code**
> software tool created by HashiCorp.

You can specify your infrastructure using the terraform languague and then `apply` it.

--

Pros:
- You don't have to fiddle with the UI anymore
- You can version your infrastructure
- You can create and destroy complex infrastructure using 2 commands
- It makes your infrastructure reproducibile (test/staging/prod/...)

Cons:
- It's something new to learn
- Managing `states` can be painful

--

### Terraform & Basil

Inside our `infrastructure/`

```bash
cluster # where we define our cluster
main.tf # entry point
kubernetes # yaml files for kubernetes
terraform.tfvars # hardcoded variables
variables.tf # variables needed by our infrastructure
```

--

### Terraform & Basil

Inside our `infrastructure/cluster`

```bash
gke.tf # GKE cluster specification
ingress.tf # ingress is a Kubernetes object
           # it exposes our application behind a LB
network.tf # VPC and subnetworks
roles.tf # new role inside our Kubernetes cluster
variables.tf # variables needed by our infrastructure
```

--

### Create our infrastructure

There are 4 commands that we need to use

- `terraform init`
- `terraform validate`
- `terraform plan`
- `terraform apply`

time to `cd infrastructure/`

--

Time to initialize `terraform`

```bash
terraform init
```

--

Validate is used to validate our `.tf` files

```bash
terraform validate
```

```output
Success! The configuration is valid.
```

--

Now let's plan

```bash
terraform plan -out=tfplan -input=false
```

The output is the graph of the resources terraform plans to create.

--

The last command is `apply`, to create our infrastructure.

```bash
terraform apply -input=false tfplan
```

It should take less than 10 minutes.

--

```output
Apply complete! Resources: 8 added, 0 changed, 0 destroyed.

The state of your infrastructure has been saved to the path
below. This state is required to modify and destroy your
infrastructure, so keep it safe. To inspect the complete state
use the `terraform show` command.

State path: terraform.tfstate
```

--

The `terraform.tfstate` contains the state ouf our infrastructure.

Generally speaking we should not touch it, especially if you want to avoid drift states.

> Drift: real-world state of your infrastructure
> differs from the state defined in your configuration

--

Check your gcp console!

What do you see?

--

The last step is to destroy what we just created

```bash
terraform destroy
```

```out
Destroy complete! Resources: 8 destroyed.
```

The `terraform.tfstate` should be empty now.

--

# Q&A Session
