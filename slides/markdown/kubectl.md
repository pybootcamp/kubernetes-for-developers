# Kubectl, the Kubernetes CLI

--

### Bring back our cluster

From the `infrastructure` folder

```
terraform plan -out=tfplan -input=false
terraform apply -input=false tfplan
```

--

### Get credentials to use kubectl

We need to download the `~/.kube/config` for our cluster using `gcloud`

```bash
gcloud container clusters get-credentials \
    k8s-cluster-staging --region europe-west1-b
```

Test `kubectl` with

```bash
kubectl get all
```

--

# Imperative vs Declarative

--

### Imperative

> When using imperative commands,
> a user operates directly on live objects in a cluster.

```
kubectl create deployment nginx --image nginx
```

let's check the new resource that we have created

```
kubectl get all
```

We have a `pod`, a `replicaset` and a `deployment`

--

Congrats 🎉🎉🎉

You just created a `deployment` resource in an `imperative` way.

--

Let's remove the `Deployment`

```
kubectl delete deploy nginx
```

and run `get all` again

```
kubectl get all
```

--

### Declarative

> When using declarative object configuration, a user operates on object configuration files stored locally, however the user does not define the operations to be taken on the file

--

We will use the same command but we will add `--dry-run -o yaml` thus:

- the command is run in dry run mode, so no objects are created
- it will display the object configuration

--

```
kubectl create deployment nginx \
    --dry-run -o yaml --image nginx
```

and we can redirect the output to a new file

```
kubectl create deployment nginx \
    --dry-run -o yaml --image nginx > deploy.yaml
```

And create a new resource in a `declarative way`:

```
kubectl apply -f deploy.yaml
```

To delete the objects

```
kubectl delete -f deploy.yaml
```

--

### To sum up

`The preferred approach for managing Resources is through declarative files` and `Apply is the preferred mechanism for managing Resources in a Kubernetes cluster.`

[Kubectl documentation](https://kubectl.docs.kubernetes.io/)

--

### Use kubectl to introspect our cluster

How many nodes do we have?

```bash
kubectl get nodes
```

we can also get more information using `-o wide`

```
kubectl get nodes -o wide
```

We can use `describe` to get more information

```
kubectl describe node YOUR_NODE_NAME_HERE
```

--

Then look for `Labels` inside the `describe` output

```output
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/fluentd-ds-ready=true
                    beta.kubernetes.io/instance-type=g1-small
                    beta.kubernetes.io/os=linux
                    cloud.google.com/gke-nodepool=nodes
                    cloud.google.com/gke-os-distribution=cos
                    failure-domain.beta.kubernetes.io/region=europe-west1
                    failure-domain.beta.kubernetes.io/zone=europe-west1-b
                    kubernetes.io/hostname=gke-k8s-cluster-staging-nodes-78017c4d-mq77
                    preemptible=false
```

--

We can filter using `labels`

```
kubectl get nodes -l preemptible=true
```

--

We can use `describe` on different resources

```bash
kubectl describe deploy/nginx
```

--

Inside the output there is a `Pod Template` part

```output
Pod Template:
```

But what is a Pod?

--

> A Pod is the basic execution unit of a Kubernetes application
> the smallest and simplest unit in the Kubernetes object model that you create or deploy.

--

A pod can have 1 or multiple containers.

```bash
kubectl get pods
```

We can also run `describe` on our pod

```bash
kubectl describe pod YOUR_POD_NAME
```

--

### imperative way

Before deploying `basil` we need to deploy `redis`

```
kubectl create deployment redis-web \
    --image redis:5.0-alpine3.8
```

and then expose it (create a service resource)

```
kubectl expose deploy redis-web \
    --port=6379 --target-port=6379
```

--

### Create basil

```bash
kubectl run basil-web --image=pybootcamp/basil:0.1 \
    --labels="app=basil,tier=web" \
    --env="REDIS_URI=redis://redis-web:6379" \
    --port=8080
```

--

### Does it work?

We can check logs

```bash
kubectl logs -l app=basil,tier=web
```

--

### Expose basil

```
kubectl expose deployment basil-web \
    --type='NodePort' -l="app=basil" \
    --port=8080 --target-port=8080
```

In a couple of minutes we should see the `ingress/k8s-cluster-staging` resource getting an IP address.

Using

```
kubectl get ingress
```

we can check the IP address.

--

### Why does not work?

```
kubectl describe ingress
```

--

The GKE ingress expects the `service` to be `HEALTHY`.

Your application either:

- returns a `200` in case of a request on `/`
- specify a `livenessProbe` and `readinessProbe` inside your spec

Otherwise your service will be `UNHEALTHY` and not available from the outside.

--

### declarative way

Remove the `service` and `deployment`

```bash
kubectl delete svc basil-web redis-web
kubectl delete deploy basil-web redis-web
```

Apply our resource configurations

```bash
kubectl apply -f kubernetes/service.yaml
kubectl apply -f kubernetes/deploy.yaml
```

--

### And after a couple of minutes

The application shoud be `HEALTHY`

```
kubectl describe ingress
```

--

# Add some events

We can use `curl`, the `/docs` endpoint, or create a cronjob to add more events.

```bash
kubectl apply -f kubernetes/job.yaml
```

--

# Q&A Session
