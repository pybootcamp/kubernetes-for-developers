# Google Kubernetes Engine

--

### Google Kubernetes Engine

Kubernetes Engine is a **managed**, **production-ready** environment for deploying containerized applications.

> Long story short you select the number of nodes,
> press create and you get a Kubernetes cluster.

--

### Main features

- Master nodes are completely managed by Google
- Node updates are managed by Google
- Well integrated with the rest of their services and infrastructure
- GPU support
- Seamlessly integration with Stackdriver logging and monitoring
- It's free, you pay only for the nodes (Compute Engine)

--

### Running your own cluster VS using GKE

Given that:

- Kubernetes is a really complex piece of software made by many components
- Distributed systems are hard to run
- Provinding HA for you production system is a full time job

Why would you run your own cluster?

--

- You are Github and you run [Kubernetes on bare metal](https://github.blog/2017-08-16-kubernetes-at-github/)
- You are a cloud provider
- You want to have full control over the master nodes/master plan
- You don't use cloud solutions
- You need to run the last version of Kubernetes
- ????

--

### Why running your cluster inside GKE is a good idea

Using GKE removes lots of daily overhead related to running Kubernetes.

That applies also to the other cloud providers.

--

You get (almost) the same flexibility you would get by running your own cluster without having to deal with the pain points of running Kubernetes.

You can build and run your applications, without the pain of having to know all the nitty-gritty details of Kubernetes.

Last but not least GKE is well integrated with Terraform.

--

# Q&A Session
