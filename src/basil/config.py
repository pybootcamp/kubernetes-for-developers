import logging
import logging.config
import os

import structlog
import structlog._frames

EVENTS = "events"
REDIS_URI = os.getenv("REDIS_URI", "redis://localhost")
CONTAINER_NAME = os.getenv("HOSTNAME", "NA")


def logging_setup():
    pre_chain = [
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_logger_name,
        structlog.processors.TimeStamper(fmt="iso"),
    ]

    structlog.configure(
        processors=pre_chain
        + [
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        context_class=structlog.threadlocal.wrap_dict(dict),
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )

    processor = structlog.processors.JSONRenderer(sort_keys=True)
    if os.getenv("DEBUG", False):
        processor = structlog.dev.ConsoleRenderer()

    LOGGING = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "plain": {
                "()": structlog.stdlib.ProcessorFormatter,
                "processor": processor,
                "foreign_pre_chain": pre_chain,
            }
        },
        "handlers": {
            "default": {
                "level": logging.INFO,
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",
                "formatter": "plain",
            }
        },
        "loggers": {
            "": {"level": "INFO", "handlers": ["default"], "propagate": False},
            "basil": {
                "level": logging.INFO,
                "propagate": False,
                "handlers": ["default"],
            },
            "uvicorn": {
                "level": logging.INFO,
                "propagate": False,
                "handlers": ["default"],
            },
        },
    }

    logging.config.dictConfig(LOGGING)
