from pydantic import BaseModel


class Data(BaseModel):
    value: int


class Payload(BaseModel):
    timestamp: int
    sensor: str
    data: Data
